const config = require('./webpack.config.js')
const express = require('express')
const webpack = require('webpack')
const wdm = require('webpack-dev-middleware')
const whc = require('webpack-hot-client')

const compiler = webpack(config)

const app = express()

whc(compiler)

app.use(wdm(compiler, {
    publicPath: config.output.publicPath,
    stats: {
        colors: true
    }
}))

app.listen(3000, 'localhost', err => {
    if (err) throw err
    console.log('Server started at http://localhost:3000')
})