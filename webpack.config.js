const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const {VueLoaderPlugin} = require('vue-loader')
const config = {
    entry: [ path.resolve(__dirname, 'src', 'index.js') ],
    
    mode: 'development',

    devtool: '#cheap-module-eval-source-map',
    
    output: {
        publicPath: '/',
        filename: '[name].[hash].js'
    },

    resolve: {
        extensions: ['.js', '.vue', '.json'],
        alias: {
            '@': path.resolve(__dirname, 'src')
        }
    },

    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                include: [path.resolve(__dirname, 'src')]
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader', 'css-loader', 'postcss-loader'
                ]
            },
            {
                test: /\.(jpe?g|png|gif|ico|eot|ttf|woff|woff2|svg|svgz)$/i,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            name: '[name].[ext]'
                        }
                    }
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    {
                        loader: 'sass-loader'
                    }
                ]
            }
        ]
    },
    
    plugins: [
        new VueLoaderPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.ProgressPlugin(),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'index.html',
            inject: true
        })
    ]
}

module.exports = config